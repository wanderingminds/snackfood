# README #

Some python snackfood and brainteasers.

### What is this repository for? ###

* Relatively simple jupyter notebooks on small topics
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* A jupyter notebook installation
  * Install anaconda notebook extensions (optional): nb_conda
  * Install jupyter-notebook extensions:
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

conda create -n py36 python=3.6 anaconda
conda update conda
source ...
conda update anaconda
conda install pint vispy


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact